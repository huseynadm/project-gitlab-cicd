FROM maven:3.8.3-openjdk-17
COPY JavaProject/target/JavaProject-1.0-SNAPSHOT.jar .
CMD ["java","-cp JavaProject-1.0-SNAPSHOT.jar org.example.Main"]
